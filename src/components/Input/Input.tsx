import { FC, InputHTMLAttributes, memo } from 'react'
import classNames from 'classnames'

type Props = InputHTMLAttributes<HTMLInputElement> & {
  label?: string
}
const Input: FC<Props> = ({ className = '', label, ...props }) => {
  const cn = classNames('input', className)
  return (
    <label htmlFor={props.id}>
      {label?.length && (
        <p className="leading-7 mb-2 sm:text-sm">
          {label}
          {props?.required && <span className="text-primary ml-1 text-lg">*</span>}
        </p>
      )}
      <input {...props} className={cn} />
    </label>
  )
}

export default memo(Input)